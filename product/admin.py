from django.contrib import admin

from .models import Product, VendingMachine, Category

admin.site.register(Product)
admin.site.register(VendingMachine)
admin.site.register(Category)