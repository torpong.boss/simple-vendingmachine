from rest_framework import serializers

from .models import Product, VendingMachine, Category

class VendingMachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendingMachine
        fields = (
					'location', 
				)

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
					'name', 
					'image_link',
					'created_at' ,
				)

class ProductSerializer(serializers.ModelSerializer):
	category = CategorySerializer()
	location = VendingMachineSerializer()

	class Meta:
		model = Product
		fields = (
			'name',
			'quantity',
			'price',
			'created_at',
			'image_link',
			'category',
			'location'
		)
