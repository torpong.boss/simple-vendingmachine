from django.urls import path, include

from product import views

urlpatterns = [
  path('latest-products/', views.LatestProductsList.as_view()),
  path('products/search/', views.search),
  path('all-categories', views.AllCategories.as_view()),
  path('get-locations', views.GetLocation.as_view()),
  path('products/buy', views.buy)
]