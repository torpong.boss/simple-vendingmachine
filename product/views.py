from django.shortcuts import render
from django.db.models import Q
from django.db.models import F

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .models import Product, VendingMachine, Category
from .serializers import ProductSerializer, CategorySerializer, VendingMachineSerializer

class GetLocation(APIView):
	def get(self, request, format=None):
		vending_machine = VendingMachine.objects.all()
		serializer = VendingMachineSerializer(vending_machine, many=True)
		return Response(serializer.data)

class AllCategories(APIView):
	def get(self, request, format=None):
		category = Category.objects.all()
		serializer = CategorySerializer(category, many=True)
		return Response(serializer.data)

class LatestProductsList(APIView):
	def get(self, request, format=None):
		products = Product.objects.all()[0:4]
		serializer = ProductSerializer(products, many=True)
		return Response(serializer.data)


@api_view(['POST'])
def search(request):
	category = request.data.get('category', '')
	location = request.data.get('location', '')
	if category:
		products = Product.objects.filter(category__name=category, location__location=location)
		serializer = ProductSerializer(products, many=True)
		return Response(serializer.data)
	else:
		return Response({"products": []})

@api_view(['POST'])
def buy(request):
	name = request.data.get('name', '')
	location = request.data.get('location', '')

	products = Product.objects.get(name=name,location__location=location)
	products.save()
	
	serializer = ProductSerializer(products)
	return Response(serializer.data)