from django.db import models
from django.core.mail import send_mail

class Category(models.Model):
	name = models.CharField(max_length=255)
	image_link = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ('name',)

	def __str__(self):
		return self.name

class VendingMachine(models.Model):
	location = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ('location',)

	def __str__(self):
		return self.location

class Product(models.Model):
	category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
	location = models.ForeignKey(VendingMachine, related_name='products', on_delete=models.CASCADE, null=True)
	name = models.CharField(max_length=255)
	quantity = models.IntegerField()
	price = models.DecimalField(max_digits=16, decimal_places=2)
	created_at = models.DateTimeField(auto_now_add=True)
	image_link = models.CharField(max_length=255)

	def save(self, *args, **kwargs):
		if self.quantity-1 >= 0:
			self.quantity = self.quantity-1
			super(Product, self).save(*args, **kwargs)
			if self.quantity-1 < 10:
				abLocation = VendingMachine.objects.get(location=self.location)
				send_mail(
					'Warning about running out of stock',
					self.name + ' is running out ' + 'at ' + abLocation.location,
					'torpong.boss@gmail.com',
					['torpong.boss@gmail.com'],
					fail_silently=False,
				)

	class Meta:
		ordering = ('name',)

	def __str__(self):
		return self.name

